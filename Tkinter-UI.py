#! /usr/bin/python3


import pathlib
import tkinter as tk

from tkinter import filedialog as tkf
from tkinter import Tk
from tkinter.messagebox import showinfo

import dataprocessor as trk

class Interface(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.parent = master
        self.grid(sticky=tk.N+tk.S+tk.E+tk.W)
        self.__create_widgets()
    def __create_widgets(self):
        crow = 0 # Current row
        
        tk.Label(self, text='Fichier .TRK').grid(row=crow, column=0)
        self.infile_path = tk.StringVar(self)
        infile_field = tk.Entry(self, textvariable=self.infile_path)
        infile_field.grid(row=crow, column=1)
        tk.Button(self,
                  text='...',
                  command=self.ask_trk_path).grid(row=crow, column=2)
        
        crow += 1
        tk.Label(self, text='Nom du fichier à créer').grid(row=crow, column=0)
        self.gpx_file_path = tk.StringVar(self)
        gpx_field = tk.Entry(self, textvariable=self.gpx_file_path)
        gpx_field.grid(row=crow, column=1)
        tk.Button(self,
                  text='...',
                  command=self.ask_gpx_path).grid(row=crow, column=2)
        
        crow += 1
        tk.Button(self,
                  text='Convertir',
                  command=self.convert).grid(row=crow, column=2)
        tk.Button(self,
                  text='Quitter',
                  command=self.parent.destroy).grid(row=crow, column=0)
        
    def ask_trk_path(self):
        self.infile_path.set(
                tkf.askopenfilename(defaultextension='.TRK',
                                    filetypes=[('Track', '.TRK')],
                                    title='Choix du fichier trk en entrée')
                )
                
    def ask_gpx_path(self):
        default_gpx_name = self.infile_path.get()
        default_gpx_name = pathlib.PurePath(default_gpx_name).stem + '.gpx'
        self.gpx_file_path.set(
                tkf.asksaveasfilename(title='Enregistrer sous',
                                      defaultextension='.gpx',
                                      filetypes=[('GPS Exchange Format', '.gpx')],
                                      initialfile=default_gpx_name)
                )
                
    def convert(self):
        trk_file = self.infile_path.get()
        gpx_file = self.gpx_file_path.get()
        
        data = trk.read_trk_file(trk_file)
        df = trk.trk_data_to_df(data)
        trk.df_to_gpx(df, gpx_file)
        showinfo('Conversion terminée', 'La conversion du fichier est terminée')

if __name__ == '__main__':
    app = Tk()
    iface = Interface(app)
    iface.mainloop()